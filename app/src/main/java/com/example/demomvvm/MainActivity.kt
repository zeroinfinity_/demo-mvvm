package com.example.demomvvm

import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.example.demomvvm.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var number = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvNumber.text = number.toString()
        binding.btnFeed.setOnClickListener {
            number++
            animateYoshi()
        }

        animateYoshi()
    }

    private fun animateYoshi() {
        with(binding) {
            tvNumber.text = number.toString()

            when (number) {
                0 -> imgYoshiEgg.visibility = View.VISIBLE
                in 1..5 -> animateView(imgYoshiEgg)
                in 6..10 -> {
                    imgYoshiEgg.visibility = View.GONE
                    imgBabyYoshi.visibility = View.VISIBLE
                    animateView(imgBabyYoshi)
                }
                else -> {
                    imgBabyYoshi.visibility = View.GONE
                    imgYoshi.visibility = View.VISIBLE
                    animateView(imgYoshi)
                }
            }
        }
    }

    private fun animateView(view: View) {
        val shake: Animation = AnimationUtils.loadAnimation(this, R.anim.shake)
        view.startAnimation(shake)
    }
}
